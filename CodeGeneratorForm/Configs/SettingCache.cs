﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGeneratorForm.Configs
{
    [Serializable]
    public class SettingCache
    {
        public string IpAddress { get; set; }
        public string DatabaseName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PackageName { get; set; }
        public string Path { get; set; }
        public string tempPath { get; set; }

        public bool Domain { get; set; }
        public bool Mapper { get; set; }
        public bool ServiceInterface { get; set; }
        public bool ServiceImpl { get; set; }
        public bool Controller { get; set; }
        public bool Query { get; set; }
    }
}
