﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodeGeneratorForm
{
    public class DBHelper
    {

        public static string IpAddress { get; set; }
        public static string DatabaseName { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }

        public static string GetConnStr()
        {
            return $"server={IpAddress};database={DatabaseName};username={UserName};password={Password};";
        }

        public static void TestConnection()
        {
            using MySqlConnection conn = new MySqlConnection(GetConnStr());
            conn.Open();
        }

        public static DataTable Query(string sql)
        {
            using MySqlConnection conn = new MySqlConnection(GetConnStr());
            using MySqlDataAdapter da = new MySqlDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        public static List<T> Query<T>(string sql) where T : class
        {
            using MySqlConnection conn = new MySqlConnection(GetConnStr());
            using MySqlCommand comm = new MySqlCommand(sql, conn);

            Type type = typeof(T);
            PropertyInfo[] propertyInfos = type.GetProperties().Where(m => m.Name != "Columns").ToArray();
            var result = new List<T>();

            conn.Open();
            var source = comm.ExecuteReader();
            string[] columnName = source.GetColumnSchema().Select(m => m.ColumnName.ToLower()).ToArray();
            while (source.Read())
            {
                T entity = Activator.CreateInstance<T>();
                foreach (var prop in propertyInfos)
                {
                    if (!columnName.Contains(prop.Name.ToLower())) continue;
                    prop.SetValue(entity, source[prop.Name] == DBNull.Value ? null : source[prop.Name]);
                }
                result.Add(entity);
            }
            return result;
        }


        public static bool IsFundamental(Type type)
        {
            return type.IsPrimitive || type.IsEnum || type.Equals(typeof(string)) || type.Equals(typeof(DateTime));
        }

    }
}
