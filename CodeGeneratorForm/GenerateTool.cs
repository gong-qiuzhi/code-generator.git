﻿using CodeGeneratorModels.MySql;
using JinianNet.JNTemplate;
using log4net;
using Microsoft.VisualBasic.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGeneratorForm
{
    public class GenerateTool
    {
        private static Dictionary<string, string> dict;

        public static bool IsBuilderDomain = true;
        public static bool IsBuilderMapper = true;
        public static bool IsBuilderService = true;
        public static bool IsBuilderServiceImpl = true;
        public static bool IsBuilderController = true;
        public static bool IsBuilderQuery = false;

        static GenerateTool()
        {
            dict = new Dictionary<string, string>
            {
                { "bigint", "Long" },
                { "varchar", "String" },
                { "int", "Integer" },
                { "char", "String" },
                { "text", "String" },
                { "longtext", "String" },
                { "float", "Float" },
                { "date", "Date" },
                { "datetime", "Date" },
                { "time", "Timestamp" },
                { "bit", "Boolean" },
                { "tinyint", "Boolean" },
                { "double", "Double" },
                { "decimal", "BigDecimal" }
            };

        }

        public static void Builder(string path, string tempPath, string packageName, List<TableStruct> tables)
        {
            #region 配置处理
            //table.Name = table.Name.StartsWith("t_") ? table.Name.Replace("t_", "") : table.Name;//去除t_开头的表名

            //Engine.Configure(config =>
            //{
            //    config.OutMode = OutMode.StripWhiteSpace;
            //});
            

            if (tables == null || tables.Count == 0)
                throw new SystemException("没有选择生成的表!");

            Program.logger.Debug("生成了:" + string.Join(',', tables.Select(m => m.Name)));

            string basePath = path + "\\main\\java\\" + packageName.Replace(".", "\\");
            string templatePath = Application.StartupPath + $"template\\{tempPath}\\";
            #endregion

            #region domain层
            if (IsBuilderDomain)
            {
                string domainPath = basePath + "\\domain";
                Directory.CreateDirectory(domainPath);
                foreach (var table in tables)
                {
                    var template = Engine.LoadTemplate(templatePath + "domain.txt");
                    template.Set("table", table);
                    template.Set("dict", dict);
                    template.Set("package", packageName);
                    var content = template.Render();
                    string filePath = domainPath + "\\" + table.ClassName + ".java";
                    File.WriteAllText(filePath, content);
                }
            }
            #endregion

            #region mapper层
            if (IsBuilderMapper)
            {
                string mapperPath = basePath + "\\mapper";
                Directory.CreateDirectory(mapperPath);
                foreach (var table in tables)
                {
                    var template = Engine.LoadTemplate(templatePath + "mapper.txt");
                    template.Set("package", packageName);
                    template.Set("table", table);
                    var content = template.Render();
                    string filePath = mapperPath + "\\" + table.ClassName + "Mapper.java";
                    File.WriteAllText(filePath, content);
                }
            }
            #endregion

            #region service接口层
            if (IsBuilderService)
            {
                string servicePath = basePath + "\\service";
                Directory.CreateDirectory(servicePath);
                foreach (var table in tables)
                {
                    var template = Engine.LoadTemplate(templatePath + "serviceinterface.txt");
                    template.Set("package", packageName);
                    template.Set("table", table);
                    var content = template.Render();
                    string filePath = servicePath + "\\I" + table.ClassName + "Service.java";
                    File.WriteAllText(filePath, content);
                }
            }
            #endregion

            #region service实现层
            if (IsBuilderServiceImpl)
            {
                string serviceImplPath = basePath + "\\service\\impl";
                Directory.CreateDirectory(serviceImplPath);
                foreach (var table in tables)
                {
                    var template = Engine.LoadTemplate(templatePath + "serviceImpl.txt");
                    template.Set("package", packageName);
                    template.Set("table", table);
                    var content = template.Render();
                    string filePath = serviceImplPath + "\\" + table.ClassName + "ServiceImpl.java";
                    File.WriteAllText(filePath, content);
                }
            }
            #endregion

            #region controller层
            if (IsBuilderController)
            {
                string controllerPath = basePath + "\\controller";
                Directory.CreateDirectory(controllerPath);
                foreach (var table in tables)
                {
                    var template = Engine.LoadTemplate(templatePath + "controller.txt");
                    template.Set("package", packageName);
                    template.Set("table", table);
                    var content = template.Render();
                    string filePath = controllerPath + "\\" + table.ClassName + "Controller.java";
                    File.WriteAllText(filePath, content);
                }
            }
            #endregion

            #region query层
            if (IsBuilderQuery)
            {
                string queryPath = basePath + "\\query";
                Directory.CreateDirectory(queryPath);
                foreach (var table in tables)
                {
                    var template = Engine.LoadTemplate(templatePath + "query.txt");
                    template.Set("package", packageName);
                    template.Set("table", table);
                    var content = template.Render();
                    string filePath = queryPath + "\\" + table.ClassName + "Query.java";
                    File.WriteAllText(filePath, content);
                }
            }
            #endregion

        }

    }
}
