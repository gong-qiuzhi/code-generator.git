﻿namespace CodeGeneratorForm
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.databaseNameTxt = new System.Windows.Forms.TextBox();
            this.userNameTxt = new System.Windows.Forms.TextBox();
            this.passwordTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ipAddressTxt = new System.Windows.Forms.TextBox();
            this.testConnBtn = new System.Windows.Forms.Button();
            this.mySqlCommand1 = new MySql.Data.MySqlClient.MySqlCommand();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.GetTable = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tableNameTxt = new System.Windows.Forms.TextBox();
            this.searchBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.builderBtn = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.packageNameTxt = new System.Windows.Forms.TextBox();
            this.packageNameLable = new System.Windows.Forms.Label();
            this.pathTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.queryCheck = new System.Windows.Forms.CheckBox();
            this.controllerCheck = new System.Windows.Forms.CheckBox();
            this.serviceImplCheck = new System.Windows.Forms.CheckBox();
            this.serviceInterFaceCheck = new System.Windows.Forms.CheckBox();
            this.mapperCheck = new System.Windows.Forms.CheckBox();
            this.domainCheck = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.refresh = new System.Windows.Forms.Button();
            this.templateCom = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 116);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "数据库:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "用户名:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(62, 236);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 31);
            this.label3.TabIndex = 1;
            this.label3.Text = "密码:";
            // 
            // databaseNameTxt
            // 
            this.databaseNameTxt.Location = new System.Drawing.Point(157, 113);
            this.databaseNameTxt.Name = "databaseNameTxt";
            this.databaseNameTxt.Size = new System.Drawing.Size(307, 38);
            this.databaseNameTxt.TabIndex = 2;
            this.databaseNameTxt.Text = "pethome";
            // 
            // userNameTxt
            // 
            this.userNameTxt.Location = new System.Drawing.Point(157, 173);
            this.userNameTxt.Name = "userNameTxt";
            this.userNameTxt.Size = new System.Drawing.Size(307, 38);
            this.userNameTxt.TabIndex = 2;
            this.userNameTxt.Text = "root";
            // 
            // passwordTxt
            // 
            this.passwordTxt.Location = new System.Drawing.Point(157, 233);
            this.passwordTxt.Name = "passwordTxt";
            this.passwordTxt.Size = new System.Drawing.Size(307, 38);
            this.passwordTxt.TabIndex = 2;
            this.passwordTxt.Text = "123456";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(62, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 31);
            this.label4.TabIndex = 0;
            this.label4.Text = "主机:";
            // 
            // ipAddressTxt
            // 
            this.ipAddressTxt.Location = new System.Drawing.Point(157, 53);
            this.ipAddressTxt.Name = "ipAddressTxt";
            this.ipAddressTxt.Size = new System.Drawing.Size(307, 38);
            this.ipAddressTxt.TabIndex = 2;
            this.ipAddressTxt.Text = "localhost";
            // 
            // testConnBtn
            // 
            this.testConnBtn.Location = new System.Drawing.Point(55, 298);
            this.testConnBtn.Name = "testConnBtn";
            this.testConnBtn.Size = new System.Drawing.Size(152, 57);
            this.testConnBtn.TabIndex = 3;
            this.testConnBtn.Text = "测试连接";
            this.testConnBtn.UseVisualStyleBackColor = true;
            this.testConnBtn.Click += new System.EventHandler(this.testConnBtn_Click);
            // 
            // mySqlCommand1
            // 
            this.mySqlCommand1.CacheAge = 0;
            this.mySqlCommand1.Connection = null;
            this.mySqlCommand1.EnableCaching = false;
            this.mySqlCommand1.Transaction = null;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 82;
            this.dataGridView1.RowTemplate.Height = 40;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(766, 807);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // GetTable
            // 
            this.GetTable.Location = new System.Drawing.Point(276, 298);
            this.GetTable.Name = "GetTable";
            this.GetTable.Size = new System.Drawing.Size(152, 57);
            this.GetTable.TabIndex = 3;
            this.GetTable.Text = "获取表";
            this.GetTable.UseVisualStyleBackColor = true;
            this.GetTable.Click += new System.EventHandler(this.GetTable_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 82;
            this.dataGridView2.RowTemplate.Height = 40;
            this.dataGridView2.Size = new System.Drawing.Size(873, 807);
            this.dataGridView2.TabIndex = 5;
            // 
            // tableNameTxt
            // 
            this.tableNameTxt.Location = new System.Drawing.Point(666, 76);
            this.tableNameTxt.Name = "tableNameTxt";
            this.tableNameTxt.Size = new System.Drawing.Size(188, 38);
            this.tableNameTxt.TabIndex = 2;
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(886, 71);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(147, 46);
            this.searchBtn.TabIndex = 6;
            this.searchBtn.Text = "查询";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.searchBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(574, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 31);
            this.label5.TabIndex = 7;
            this.label5.Text = "表名:";
            // 
            // builderBtn
            // 
            this.builderBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.builderBtn.Location = new System.Drawing.Point(2070, 71);
            this.builderBtn.Name = "builderBtn";
            this.builderBtn.Size = new System.Drawing.Size(147, 46);
            this.builderBtn.TabIndex = 6;
            this.builderBtn.Text = "一键生成";
            this.builderBtn.UseVisualStyleBackColor = true;
            this.builderBtn.Click += new System.EventHandler(this.builderBtn_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(574, 165);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dataGridView2);
            this.splitContainer1.Size = new System.Drawing.Size(1643, 807);
            this.splitContainer1.SplitterDistance = 766;
            this.splitContainer1.TabIndex = 8;
            // 
            // packageNameTxt
            // 
            this.packageNameTxt.Location = new System.Drawing.Point(1150, 73);
            this.packageNameTxt.Name = "packageNameTxt";
            this.packageNameTxt.Size = new System.Drawing.Size(326, 38);
            this.packageNameTxt.TabIndex = 2;
            this.packageNameTxt.Text = "cn.itsource";
            // 
            // packageNameLable
            // 
            this.packageNameLable.AutoSize = true;
            this.packageNameLable.Location = new System.Drawing.Point(1058, 76);
            this.packageNameLable.Name = "packageNameLable";
            this.packageNameLable.Size = new System.Drawing.Size(68, 31);
            this.packageNameLable.TabIndex = 7;
            this.packageNameLable.Text = "包名:";
            // 
            // pathTxt
            // 
            this.pathTxt.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pathTxt.Location = new System.Drawing.Point(1579, 73);
            this.pathTxt.Name = "pathTxt";
            this.pathTxt.Size = new System.Drawing.Size(453, 38);
            this.pathTxt.TabIndex = 2;
            this.pathTxt.Text = "E:\\\\";
            this.pathTxt.DoubleClick += new System.EventHandler(this.pathTxt_DoubleClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1490, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 31);
            this.label6.TabIndex = 7;
            this.label6.Text = "路径:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.databaseNameTxt);
            this.groupBox1.Controls.Add(this.ipAddressTxt);
            this.groupBox1.Controls.Add(this.userNameTxt);
            this.groupBox1.Controls.Add(this.GetTable);
            this.groupBox1.Controls.Add(this.passwordTxt);
            this.groupBox1.Controls.Add(this.testConnBtn);
            this.groupBox1.Location = new System.Drawing.Point(34, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(517, 398);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "连接配置 ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.queryCheck);
            this.groupBox2.Controls.Add(this.controllerCheck);
            this.groupBox2.Controls.Add(this.serviceImplCheck);
            this.groupBox2.Controls.Add(this.serviceInterFaceCheck);
            this.groupBox2.Controls.Add(this.mapperCheck);
            this.groupBox2.Controls.Add(this.domainCheck);
            this.groupBox2.Location = new System.Drawing.Point(34, 501);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(517, 261);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "生成包配置";
            // 
            // queryCheck
            // 
            this.queryCheck.AutoSize = true;
            this.queryCheck.Checked = true;
            this.queryCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.queryCheck.Location = new System.Drawing.Point(294, 200);
            this.queryCheck.Name = "queryCheck";
            this.queryCheck.Size = new System.Drawing.Size(112, 35);
            this.queryCheck.TabIndex = 0;
            this.queryCheck.Text = "query";
            this.queryCheck.UseVisualStyleBackColor = true;
            // 
            // controllerCheck
            // 
            this.controllerCheck.AutoSize = true;
            this.controllerCheck.Checked = true;
            this.controllerCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.controllerCheck.Location = new System.Drawing.Point(31, 200);
            this.controllerCheck.Name = "controllerCheck";
            this.controllerCheck.Size = new System.Drawing.Size(156, 35);
            this.controllerCheck.TabIndex = 0;
            this.controllerCheck.Text = "controller";
            this.controllerCheck.UseVisualStyleBackColor = true;
            // 
            // serviceImplCheck
            // 
            this.serviceImplCheck.AutoSize = true;
            this.serviceImplCheck.Checked = true;
            this.serviceImplCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.serviceImplCheck.Location = new System.Drawing.Point(294, 133);
            this.serviceImplCheck.Name = "serviceImplCheck";
            this.serviceImplCheck.Size = new System.Drawing.Size(175, 35);
            this.serviceImplCheck.TabIndex = 0;
            this.serviceImplCheck.Text = "serviceImpl";
            this.serviceImplCheck.UseVisualStyleBackColor = true;
            // 
            // serviceInterFaceCheck
            // 
            this.serviceInterFaceCheck.AutoSize = true;
            this.serviceInterFaceCheck.Checked = true;
            this.serviceInterFaceCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.serviceInterFaceCheck.Location = new System.Drawing.Point(31, 133);
            this.serviceInterFaceCheck.Name = "serviceInterFaceCheck";
            this.serviceInterFaceCheck.Size = new System.Drawing.Size(226, 35);
            this.serviceInterFaceCheck.TabIndex = 0;
            this.serviceInterFaceCheck.Text = "serviceInterface";
            this.serviceInterFaceCheck.UseVisualStyleBackColor = true;
            // 
            // mapperCheck
            // 
            this.mapperCheck.AutoSize = true;
            this.mapperCheck.Checked = true;
            this.mapperCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mapperCheck.Location = new System.Drawing.Point(294, 64);
            this.mapperCheck.Name = "mapperCheck";
            this.mapperCheck.Size = new System.Drawing.Size(134, 35);
            this.mapperCheck.TabIndex = 0;
            this.mapperCheck.Text = "mapper";
            this.mapperCheck.UseVisualStyleBackColor = true;
            // 
            // domainCheck
            // 
            this.domainCheck.AutoSize = true;
            this.domainCheck.Checked = true;
            this.domainCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.domainCheck.Location = new System.Drawing.Point(31, 64);
            this.domainCheck.Name = "domainCheck";
            this.domainCheck.Size = new System.Drawing.Size(132, 35);
            this.domainCheck.TabIndex = 0;
            this.domainCheck.Text = "domain";
            this.domainCheck.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.refresh);
            this.groupBox3.Controls.Add(this.templateCom);
            this.groupBox3.Location = new System.Drawing.Point(35, 796);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(516, 174);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "模板配置";
            // 
            // refresh
            // 
            this.refresh.Location = new System.Drawing.Point(353, 75);
            this.refresh.Name = "refresh";
            this.refresh.Size = new System.Drawing.Size(117, 46);
            this.refresh.TabIndex = 2;
            this.refresh.Text = "刷新";
            this.refresh.UseVisualStyleBackColor = true;
            this.refresh.Click += new System.EventHandler(this.refresh_Click);
            // 
            // templateCom
            // 
            this.templateCom.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.templateCom.FormattingEnabled = true;
            this.templateCom.Location = new System.Drawing.Point(42, 80);
            this.templateCom.Name = "templateCom";
            this.templateCom.Size = new System.Drawing.Size(277, 39);
            this.templateCom.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(2262, 1020);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.packageNameLable);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.builderBtn);
            this.Controls.Add(this.searchBtn);
            this.Controls.Add(this.pathTxt);
            this.Controls.Add(this.packageNameTxt);
            this.Controls.Add(this.tableNameTxt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "代码生成器V3.3";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label label1;
        private Label label2;
        private Label label3;
        private TextBox databaseNameTxt;
        private TextBox userNameTxt;
        private TextBox passwordTxt;
        private Label label4;
        private TextBox ipAddressTxt;
        private Button testConnBtn;
        private MySql.Data.MySqlClient.MySqlCommand mySqlCommand1;
        private DataGridView dataGridView1;
        private Button GetTable;
        private DataGridView dataGridView2;
        private TextBox tableNameTxt;
        private Button searchBtn;
        private Label label5;
        private Button builderBtn;
        private SplitContainer splitContainer1;
        private TextBox packageNameTxt;
        private Label packageNameLable;
        private TextBox pathTxt;
        private Label label6;
        private GroupBox groupBox1;
        private GroupBox groupBox2;
        private CheckBox domainCheck;
        private CheckBox mapperCheck;
        private CheckBox serviceInterFaceCheck;
        private CheckBox serviceImplCheck;
        private CheckBox controllerCheck;
        private CheckBox queryCheck;
        private GroupBox groupBox3;
        private ComboBox templateCom;
        private Button refresh;
    }
}