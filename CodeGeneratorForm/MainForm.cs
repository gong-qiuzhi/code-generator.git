using CodeGeneratorForm.Configs;
using CodeGeneratorModels.MySql;
using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Runtime.Serialization.Formatters.Binary;

namespace CodeGeneratorForm
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void testConnBtn_Click(object sender, EventArgs e)
        {
            try
            {
                SetConnStr();
                DBHelper.TestConnection();
                MessageBox.Show("连接成功!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("连接失败!" + ex.Message);
            }

        }

        private void SetConnStr()
        {
            DBHelper.IpAddress = ipAddressTxt.Text;
            DBHelper.DatabaseName = databaseNameTxt.Text;
            DBHelper.UserName = userNameTxt.Text;
            DBHelper.Password = passwordTxt.Text;
        }

        //数据库里面所有表的集合
        List<TableStruct> tableStructs;
        //所有选中的数据表集合
        List<TableStruct> currentTables = new List<TableStruct>();

        private void GetTable_Click(object sender, EventArgs e)
        {
            SetConnStr();
            string sql = "SELECT\r\nTABLE_NAME AS `Name`,\r\nTABLE_COMMENT AS `Comment`,\r\nCREATE_TIME AS CreateTime\r\nFROM\r\ninformation_schema.TABLES\r\nWHERE\r\nTABLE_SCHEMA = (SELECT DATABASE ()) ";
            //数据库里面所有表的集合
            tableStructs = DBHelper.Query<TableStruct>(sql);
            dataGridView1.DataSource = tableStructs;
        }

        private void searchBtn_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = tableStructs.Where(m => m.Name!.Contains(tableNameTxt.Text)).ToList();
        }

        //获取当前选择的表
        private void GetSelectedTable()
        {
            currentTables.Clear();
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                string? tableName = row.Cells[nameof(TableStruct.Name)].Value.ToString();
                if (tableName != null)
                {
                    string sql = $"SELECT column_name `Name`,data_type `Type`,column_comment `Comment`,is_nullable `IsNull`,(case column_key when 'PRI' then 'true' else 'false' end) `IsKey` FROM information_schema.COLUMNS WHERE table_schema='{databaseNameTxt.Text}' and table_name = '{tableName}';";

                    List<ColumnStruct> columns = DBHelper.Query<ColumnStruct>(sql);
                    TableStruct table = new TableStruct();
                    table.Name = tableName;
                    table.Columns = columns;
                    currentTables.Add(table);
                }
            }
            CheckForIllegalCrossThreadCalls = false;

        }

        private async void builderBtn_Click(object sender, EventArgs e)
        {
            string path = pathTxt.Text.Trim();
            string package = packageNameTxt.Text.Trim();
            string tempPath = templateCom.Text.Trim();

            if (tempPath.Length == 0)
            {
                MessageBox.Show("请选择对应的模板!");
                return;
            }

            builderBtn.Enabled = false;
            builderBtn.Text = "生成中..";
            try
            {
                SetBuilderConfig();
                await Task.Run(() => GenerateTool.Builder(path, tempPath, package, currentTables));
                builderBtn.Enabled = true;
                builderBtn.Text = "一键生成!";
                MessageBox.Show("生成成功!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("生成失败!" + ex.Message);
                builderBtn.Enabled = true;
                builderBtn.Text = "一键生成!";
                throw ex;
            }
        }

        private void SetBuilderConfig()
        {
            GenerateTool.IsBuilderDomain = domainCheck.Checked;
            GenerateTool.IsBuilderMapper = mapperCheck.Checked;
            GenerateTool.IsBuilderController = controllerCheck.Checked;
            GenerateTool.IsBuilderService = serviceInterFaceCheck.Checked;
            GenerateTool.IsBuilderServiceImpl = serviceInterFaceCheck.Checked;
            GenerateTool.IsBuilderQuery = queryCheck.Checked;
        }

        private void pathTxt_DoubleClick(object sender, EventArgs e)
        {
            FolderBrowserDialog folder = new FolderBrowserDialog();
            folder.Description = "选择指定的src目录";
            if (folder.ShowDialog() == DialogResult.OK)
            {
                pathTxt.Text = folder.SelectedPath;
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            #region 拦截处理
            string[] templatePaths = new DirectoryInfo(Application.StartupPath + @"template").GetDirectories().Select(m => m.Name).ToArray();
            if (templatePaths == null || templatePaths.Length == 0)
                throw new SystemException(Application.StartupPath + @"template 下未获取到任何模板!");
            templateCom.Items.AddRange(templatePaths);
            templateCom.SelectedIndex = 0;


            if (!File.Exists(Application.StartupPath + @"\Configs\Setting.txt")) return;
            #endregion

            #region 获取缓存

            //接受对方发送过来的二进制，反序列化为对象
            SettingCache cache = new SettingCache();

            using (FileStream fs = new FileStream(Application.StartupPath + @"Configs\Setting.txt", FileMode.OpenOrCreate, FileAccess.Read))
            {
                //开始反序列化
                BinaryFormatter bf = new BinaryFormatter();
                cache = (SettingCache)bf.Deserialize(fs);
            }
            #endregion

            #region 数据库连接配置
            ipAddressTxt.Text = cache.IpAddress;
            databaseNameTxt.Text = cache.DatabaseName;
            userNameTxt.Text = cache.UserName;
            passwordTxt.Text = cache.Password;
            #endregion

            #region 程序包和路径的配置
            packageNameTxt.Text = cache.PackageName;
            pathTxt.Text = cache.Path;
            #endregion

            #region 生成的包配置
            domainCheck.Checked = cache.Domain;
            mapperCheck.Checked = cache.Mapper;
            serviceImplCheck.Checked = cache.ServiceImpl;
            serviceInterFaceCheck.Checked = cache.ServiceInterface;
            controllerCheck.Checked = cache.Controller;
            queryCheck.Checked = cache.Query;
            #endregion

            #region 配置模板
            if (templatePaths.Contains(cache.tempPath))
                templateCom.Text = cache.tempPath;

            #endregion
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            SettingCache cache = new SettingCache();
            cache.IpAddress = ipAddressTxt.Text;
            cache.UserName = userNameTxt.Text;
            cache.Password = passwordTxt.Text;
            cache.DatabaseName = databaseNameTxt.Text;

            cache.PackageName = packageNameTxt.Text;
            cache.Path = pathTxt.Text;
            cache.tempPath = templateCom.Text;

            cache.Domain = domainCheck.Checked;
            cache.Mapper = mapperCheck.Checked;
            cache.ServiceInterface = serviceInterFaceCheck.Checked;
            cache.Controller = controllerCheck.Checked;
            cache.Query = queryCheck.Checked;
            cache.ServiceImpl = serviceImplCheck.Checked;

            string settingPath = Application.StartupPath + "Configs";

            if (!Directory.Exists(settingPath))
            {
                Directory.CreateDirectory(settingPath);
            }

            using (FileStream fs = new FileStream(settingPath + @"\Setting.txt", FileMode.OpenOrCreate, FileAccess.Write))
            {
                //开始序列化为对象
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, cache);
            }
        }

        private async void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count <= 0) return;
            builderBtn.Enabled = false;
            await Task.Run(() => GetSelectedTable());
            dataGridView2.DataSource = currentTables.FirstOrDefault()?.Columns;
            builderBtn.Enabled = true;
        }

        private void refresh_Click(object sender, EventArgs e)
        {
            string[] templatePaths = new DirectoryInfo(Application.StartupPath + @"template").GetDirectories().Select(m => m.Name).ToArray();
            templateCom.Items.Clear();
            if (templatePaths == null || templatePaths.Length == 0)
                throw new SystemException(Application.StartupPath + @"template 下未获取到任何模板!");

            templateCom.Items.AddRange(templatePaths);
            templateCom.SelectedIndex = 0;
        }
    }
}