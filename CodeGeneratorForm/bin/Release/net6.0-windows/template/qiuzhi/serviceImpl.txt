﻿package ${package}.service.impl;

import cn.itsource.basic.service.impl.BaseServiceImpl;
import ${package}.domain.${table.ClassName};
import ${package}.mapper.${table.ClassName}Mapper;
import ${package}.query.${table.ClassName}Query;
import ${package}.service.I${table.ClassName}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import tk.mybatis.mapper.entity.Example;

@Service
public class ${table.ClassName}ServiceImpl
        extends BaseServiceImpl<${table.ClassName}, ${table.ClassName}Query> 
        implements I${table.ClassName}Service {
    
}
