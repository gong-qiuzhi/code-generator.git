﻿package ${package}.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "$table.Name")
public class $table.ClassName {
	$foreach(item in table.Columns)
	$if(item.Comment!=null&&item.Comment.Length!=0)
	/**
	 * $item.Comment
	 */$end$if(item.IsKey=="true")${"\n	@Id"}$end
	private $dict[item.Type] $item.FieldName;
	$end
}
