﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGeneratorModels.Extension
{
    public static class StringExtension
    {
        /// <summary>
        /// 下划线转大驼峰
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToBigHump(this string str)
        {
            if (str == null || str.Length == 0) return str;
            StringBuilder sb = new StringBuilder();
            string[] arr = str.Split('_');
            for (int i = 0; i < arr.Length; i++)
            {
                sb.Append(arr[i].Substring(0, 1).ToUpper()).Append(arr[i].Substring(1));
            }
            return sb.ToString();
        }

        /// <summary>
        /// 下划线转小驼峰
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToSmallHump(this string str)
        {
            if (str == null || str.Length == 0) return str;
            StringBuilder sb = new StringBuilder();
            string[] arr = str.Split('_');
            sb.Append(arr[0].Substring(0, 1).ToLower()).Append(arr[0].Substring(1));
            for (int i = 1; i < arr.Length; i++)
            {
                sb.Append(arr[i].Substring(0, 1).ToUpper()).Append(arr[i].Substring(1));
            }
            return sb.ToString();
        }
    }
}
