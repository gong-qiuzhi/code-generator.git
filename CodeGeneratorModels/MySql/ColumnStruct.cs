﻿using CodeGeneratorModels.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGeneratorModels.MySql
{
    public class ColumnStruct
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Comment { get; set; }
        public string IsNull { get; set; }
        public string IsKey { get; set; }

        public string FieldName { get => Name.ToSmallHump(); }

    }
}
