﻿using CodeGeneratorModels.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGeneratorModels.MySql
{
    public class TableStruct
    {
        public string? Name { get; set; }
        public List<ColumnStruct>? Columns { get; set; }
        public string ClassName { get => (Name!.StartsWith("t_") ? Name.Substring(Name.IndexOf("t_") + 2, Name.Length - 2 - Name.IndexOf("t_")) : Name).ToBigHump(); }
        public string FieldName { get => ClassName.ToSmallHump(); }
    }
}
