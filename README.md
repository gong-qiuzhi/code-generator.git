# 一、拉取工具（已拉取，可跳过）

## 1.创建文件夹

先创建一个空的文件夹,在地址栏输入cmd
![输入图片说明](https://foruda.gitee.com/images/1669435477108790703/f188baf8_8881710.jpeg "1.jpg")


## 2.拉取代码生成器工具

输入: git clone https://gitee.com/gong-qiuzhi/code-generator.git  回车,等待下载完毕即可
![输入图片说明](https://foruda.gitee.com/images/1669435495571635600/8560af53_8881710.jpeg "2.jpg")
![输入图片说明](https://foruda.gitee.com/images/1669435517510258263/44d26d90_8881710.jpeg "3.jpg")


# 二、安装运行环境(只用安装一次)  

## 1.安装环境

在文件资源管理器打开,双击运行 windowsdesktop-runtime-6.0.11-win-x64.exe 文件
![输入图片说明](https://foruda.gitee.com/images/1669435567142359328/20b8527a_8881710.jpeg "4.jpg")


 

## 2.找到工具的可执行文件

从刚刚拉取到的地方找到指定路径  

...\code-generator\CodeGeneratorForm\bin\Debug\net6.0-windows

 

## 3.运行工具

双击运行打开生成工具 CodeGeneratorForm.exe，能正常打开标识环境安装成功
![输入图片说明](https://foruda.gitee.com/images/1669435616367683423/cd7bcf91_8881710.jpeg "5.jpg")


# 三、工具的使用

## 1.配置数据库连接信息

填写对应的数据库连接信息，先获取表-->再选择要生成的表

(数据库连接信息是正确的就不用修改了，填写之后可以先测试连接看看)
![输入图片说明](https://foruda.gitee.com/images/1669435636931985104/c788d5d3_8881710.jpeg "6.jpg")


## 2.配置包名，生成地址

指定相应的包名,选择项目src所在的目录 别选错了
![输入图片说明](https://foruda.gitee.com/images/1669435700002885262/801ce366_8881710.jpeg "7.jpg")


## 3.生成代码

点击一键生成,等待弹出生成成功提示框
![输入图片说明](https://foruda.gitee.com/images/1669435718793568642/2eaa2780_8881710.jpeg "8.jpg")
 

## 4.最终生成效果:
![输入图片说明](https://foruda.gitee.com/images/1669435728785830456/d07373dd_8881710.jpeg "9.jpg")
